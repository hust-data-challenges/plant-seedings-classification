import torch.optim.lr_scheduler as lr_scheduler
import math


def cyclical_lr(stepsize, min_lr=3e-2, max_lr=3e-3):

    # Scaler: we can adapt this if we do not want the triangular CLR
    scaler = lambda x: 1.

    # Lambda function to calculate the LR
    lr_lambda = lambda it: min_lr + (max_lr - min_lr) * relative(it, stepsize)

    # Additional function to see where on the cycle we are
    def relative(it, stepsize):
        cycle = math.floor(1 + it / (2 * stepsize))
        x = abs(it / stepsize - 2 * cycle + 1)
        return max(0, (1 - x)) * scaler(cycle)

    return lr_lambda

def lambda_scheduler(optimizer, lr_lambda, **_):
    return lr_scheduler.LambdaLR(optimizer, lr_lambda)


def lr_finder_scheduler(optimizer, len_dataloaders, lr_find_epochs = 2, start_lr = 1e-7, end_lr = 1, **_):
    lr_lambda = lambda x: math.exp(x * math.log(end_lr / start_lr) / (lr_find_epochs * len_dataloaders))
    return lambda_scheduler(optimizer, lr_lambda)


def clr_scheduler(optimizer, len_dataloaders, end_lr, factor, **_):
    step_size = 4*len_dataloaders
    clr_lambda = cyclical_lr(step_size, min_lr=end_lr/factor, max_lr=end_lr)
    return lambda_scheduler(optimizer, [clr_lambda])


def step(optimizer, last_epoch, step_size=80, gamma=0.1, **_):
  return lr_scheduler.StepLR(optimizer, step_size=step_size, gamma=gamma, last_epoch=last_epoch)


def multi_step(optimizer, last_epoch, milestones=[500, 5000], gamma=0.1, **_):
  if isinstance(milestones, str):
    milestones = eval(milestones)
  return lr_scheduler.MultiStepLR(optimizer, milestones=milestones,
                                  gamma=gamma, last_epoch=last_epoch)


def exponential(optimizer, last_epoch, gamma=0.995, **_):
  return lr_scheduler.ExponentialLR(optimizer, gamma=gamma, last_epoch=last_epoch)


def none(optimizer, last_epoch, **_):
  return lr_scheduler.StepLR(optimizer, step_size=10000000, last_epoch=last_epoch)


def reduce_lr_on_plateau(optimizer, last_epoch, mode='min', factor=0.1, patience=10,
                         threshold=0.001, threshold_mode='rel', cooldown=0, min_lr=0, **_):
  return lr_scheduler.ReduceLROnPlateau(optimizer, mode=mode, factor=factor, patience=patience,
                                        threshold=threshold, threshold_mode=threshold_mode,
                                        cooldown=cooldown, min_lr=min_lr)


def cosine(optimizer, last_epoch, T_max=50, eta_min=0.00001, **_):
  print('cosine annealing, T_max: {}, eta_min: {}, last_epoch: {}'.format(T_max, eta_min, last_epoch))
  return lr_scheduler.CosineAnnealingLR(optimizer, T_max=T_max, eta_min=eta_min,
                                        last_epoch=last_epoch)


def get_scheduler(config, optimizer, last_epoch):
  func = globals().get(config.scheduler.name)
  return func(optimizer, last_epoch, **config.scheduler.params)


